# SPDX-FileCopyrightText: 2021 Tezos Commons
# SPDX-License-Identifier: AGPL-3.0-or-later

{ self }:

{ config, lib, pkgs, ... }:

let
  tzipPkgs = self.packages.${config.nixpkgs.system};
  stateDir = "tzip-web";
  cfg = config.services.tzip-web-listener;

in {

  options = {
    services.tzip-web-listener = {
      package = lib.mkOption {
        type = lib.types.path;
        default = tzipPkgs.tzip-listener;
      };

      secretFile = lib.mkOption {
        type = lib.types.path;
        description = ''
          A file containing secrets:
          - TZIP_LISTENER_GITLAB_TOKEN: A token that authenticates Gitlab requestsf
        '';
      };

      outputPath = lib.mkOption {
        type = lib.types.path;
        default = "/var/lib/${stateDir}/data";
        description = ''
          Path where the generated output will be placed.
        '';
      };

      nginxVhost = lib.mkOption {
        type = lib.types.str;
        description = ''
          Name of the nginx virtual host to configure.
        '';
      };

      repoLink = lib.mkOption {
        type = lib.types.str;
        description = "Repository for which to listen to";
        default = "https://gitlab.com/tezos/tzip";
      };

      port = lib.mkOption {
        type = lib.types.port;
        default = 12345;
        description = ''
          Port on which tzip-listener will listen
        '';
      };
    };
  };

  config = {
    systemd.services.tzip-web-listener = {
      description = ''
        Gitlab webhook listener
      '';
      wantedBy = [ "multi-user.target" ];

      path = [ cfg.package pkgs.git ];

      script = "tzip-listener";

      environment = {
        TZIP_LISTENER_SOURCE_DIR = "/tmp";
        TZIP_LISTENER_OUTPUT_DIR = cfg.outputPath;
        TZIP_LISTENER_PORT = toString cfg.port;
        TZIP_LISTENER_REPO_LINK = cfg.repoLink;
      };

      serviceConfig = {
        User = "tzip-web";
        RemainAfterExit = true;
        StateDirectory = stateDir;
        RuntimeDirectory = stateDir;
        WorkingDirectory = "/run/${stateDir}";
        EnvironmentFile = cfg.secretFile;
        PrivateTmp = true;
      };
    };

    services.nginx.virtualHosts.${cfg.nginxVhost} = {
      locations."/webhook/" = {
        proxyPass = "http://localhost:${toString cfg.port}/";
      };
    };

    users.users.tzip-web = {
      isSystemUser = true;
      description = "The user that owns tzip-web state";
      group = "tzip-web";
    };
    users.groups.tzip-web = { };
  };

}

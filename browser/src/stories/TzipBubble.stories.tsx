// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { h } from "preact";
import { defaultRepoUrl, TzipBubble } from "../common";
import {
    TzipBubbleComp,
    Props as TzipBubbleProps
} from "../components/tzipBubble/";

//const singleLineTzip: TzipBubbleProps = {
//    tzipId: "tzip-1",
//    publicPath: "/",
//    repoUrl: defaultRepoUrl,
//    tzipBubble: new TzipBubble({
//        meta: {
//            title: "A1.1 - Balanced Trees for nested or and pair types",
//            status: "Final"
//        },
//        summary: {
//            __html: "Into to FA2 - Multi-Asset Interface"
//        }
//    })
//};
//
//export default {
//    title: "Example/TzipBubble",
//    component: TzipBubbleComp,
//    argTypes: {},
//    args: singleLineTzip
//};
//
//const Template = (args: TzipBubbleProps) => () => <TzipBubbleComp {...args} />;
//
//export const SingleLineIntro = Template(singleLineTzip);
//
//export const MultiLineIntro = Template({
//    tzipId: "tzip-1",
//    publicPath: "/",
//    repoUrl: defaultRepoUrl,
//    tzipBubble: new TzipBubble({
//        meta: {
//            title: "A1.1 - Balanced Trees for nested or and pair types",
//            status: "Final"
//        },
//        summary: {
//            __html:
//                "TZIP-12 proposes a standard for a unified token contract interface,\nsupporting a wide range of token types and implementations. "
//        }
//    })
//});
//
//export const LongStatus = Template({
//    tzipId: "tzip-1",
//    publicPath: "/",
//    repoUrl: defaultRepoUrl,
//    tzipBubble: new TzipBubble({
//        meta: {
//            title: "A1.1 - Balanced Trees for nested or and pair types",
//            status: "Depriciated"
//        },
//        summary: {
//            __html:
//                "TZIP-12 proposes a standard for a unified token contract interface,\nsupporting a wide range of token types and implementations. "
//        }
//    })
//});
//
//export const LongerStatus = Template({
//    tzipId: "tzip-1",
//    publicPath: "/",
//    repoUrl: defaultRepoUrl,
//    tzipBubble: new TzipBubble({
//        meta: {
//            title: "A1.1 - Balanced Trees for nested or and pair types",
//            status: "Work in progress"
//        },
//        summary: {
//            __html:
//                "TZIP-12 proposes a standard for a unified token contract interface,\nsupporting a wide range of token types and implementations. "
//        }
//    })
//});

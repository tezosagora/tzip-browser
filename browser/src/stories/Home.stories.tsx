// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { defaultRepoUrl, TzipBubble, TzipBubbles, TzipIndex } from "../common";
import Home, { Props as HomeProps } from "../routes/home";

const sampleTzips: TzipBubbles = new Map([
    [
        "FA2 - Multi-Asset Interface",
        new TzipBubble({
            meta: {
                title: "A1.1 - Balanced Trees for nested or and pair types",
                status: "Final"
            },
            summary: {
                __html: "Into to FA2 - Multi-Asset Interface"
            }
        })
    ],
    [
        "tzip-10",
        new TzipBubble({
            meta: {
                title: "A1.1 - Balanced Trees for nested or and pair types",
                status: "Draft"
            },
            summary: {
                __html: "Into to FA2 - Multi-Asset Interface"
            }
        })
    ],
    [
        "tzip-11",
        new TzipBubble({
            meta: {
                title: "A1.1 - Balanced Trees for nested or and pair types",
                status: "Deprecated"
            },
            summary: {
                __html: "Into to FA2 - Multi-Asset Interface"
            }
        })
    ]
]);

const props: HomeProps = {
    q: "",
    publicPath: "",
    indexData: new TzipIndex(sampleTzips, new Map(), defaultRepoUrl)
};

export default {
    title: "Example/Home",
    component: Home,
    argTypes: {},
    args: props
};

// Note: Disable for now, since using react with preact does not work well with storybook 6

// const Template = (args: HomeProps) => () => <Home {...args} />;
// export const All = Template(props);

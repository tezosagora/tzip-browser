// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { FunctionalComponent, h } from "preact";
import { useEffect } from "preact/hooks";
import { DraftTzip } from "../../common";
import RenderedContainer from "../../components/rendered_container";
import { Link } from "preact-router/match";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Helmet from "react-helmet";
import { faCode } from "@fortawesome/free-solid-svg-icons/faCode";

export interface Props {
    publicPath: string;
    // We need proposal id propoerty here so that we can receive the
    // proposal id from the route.
    tzip: DraftTzip;
}

function mkPageTitle(tzip: DraftTzip) {
    return "TZIP-Draft: " + tzip.meta.title;
}

const DraftProposal: FunctionalComponent<Props> = (props: Props) => {
    return (
        <div>
            <Helmet>
                {/* This thing sets the document title. */}
                <title>{mkPageTitle(props.tzip)}</title>
            </Helmet>
            <Link href={props.publicPath} class="back-button">
                <svg width="18" height="18">
                    <g
                        fill="none"
                        fillRule="evenodd"
                        stroke="#FFF"
                        strokeWidth="2"
                    >
                        <path strokeLinejoin="round" d="M2 9h16"></path>
                        <path d="M10 17L2 9l8-8"></path>
                    </g>
                </svg>
            </Link>
            <div class="proposal-id">
                <RenderedContainer
                    class=""
                    innerHtml={props.tzip?.meta?.title}
                />

                <div class="direct-link">
                    <a
                        rel="noreferrer"
                        target="_blank"
                        href={props.tzip.directLink}
                    >
                        <FontAwesomeIcon icon={faCode} color="#444" />
                    </a>
                </div>
            </div>
            <div class="proposal-attribute">TZIP-DRAFT</div>
            <div class="proposal-attribute">
                Author: {props.tzip?.meta?.author}
            </div>
            <div class="proposal-attribute">
                Created: {props.tzip?.meta?.created}
            </div>
            <RenderedContainer
                class="proposal-container markdown-body"
                innerHtml={props.tzip?.rendered}
            />
        </div>
    );
};

export default DraftProposal;

// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { FunctionalComponent, h } from "preact";
import { Link } from "preact-router/match";

/** @jsx h */

interface Props {
    publicPath: string;
}

const NotFound: FunctionalComponent<Props> = (props: Props) => {
    return (
        <div class="nf-container">
            <div class="nf-404">404</div>
            <div class="nf-message">We couldn&apos;t find this page</div>
            <div class="nf-back-to-home">
                <Link href={props.publicPath}>
                    <h4>Back to Home</h4>
                </Link>
            </div>
        </div>
    );
};

export default NotFound;

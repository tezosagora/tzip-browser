// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */

import { FunctionalComponent, h } from "preact";
import { route } from "preact-router";
import Tzips from "../../components/tzips";
import {
    SortAttribute,
    FilterAttribute,
    FilterSortOptions,
    TzipMeta,
    TzipBubble,
    TzipIndex,
    ProposalId,
    SelectOption,
    SafeHtml
} from "../../common";
import Select from "react-select";
import Helmet from "react-helmet";

export interface Props {
    indexData: TzipIndex;
    q: string;
    publicPath: string;
}

function collectTzipAttrs(
    tzips: Map<ProposalId, TzipBubble>,
    field: Exclude<keyof TzipMeta, "discourseTopicId">
): Array<string> {
    const ret: Map<string, boolean> = new Map();

    tzips.forEach(val => {
        const v = val.meta[field];
        if (v !== undefined) {
            if (v instanceof SafeHtml) {
                ret.set(v.__html, true);
            } else {
                ret.set(v, true);
            }
        }
    });

    return Array.from(ret.keys());
}

function mkSelectValues(a: Array<string>): Array<SelectOption<string>> {
    return a.map(v => {
        return { label: v, value: v };
    });
}

const Home: FunctionalComponent<Props> = (props: Props) => {
    const fo = FilterSortOptions.from(props.q);

    const handleSortSelection = function(
        sortSelection: SelectOption<SortAttribute>
    ): void {
        fo.setSort(sortSelection.value);
        route(`${props.publicPath}q/${fo.toString()}`);
    };

    const sortOptions = mkSelectValues(
        Array.from(["Date", "Proposal ID", "Title"])
    );

    const typeOptions = mkSelectValues(
        collectTzipAttrs(props.indexData.bubbles, "type")
    );

    const statusOptions = mkSelectValues(
        collectTzipAttrs(props.indexData.bubbles, "status")
    );

    const toggleShowDrafts = () => {
        if (fo.showDrafts) {
            fo.setShowDrafts(false);
        } else {
            fo.setShowDrafts(true);
        }
        route(`${props.publicPath}q/${fo.toString()}`);
    };

    const handleChange = (fld: FilterAttribute) =>
        function(value: Array<SelectOption<string>>): void {
            if (value) {
                fo.setFilter(
                    fld,
                    value?.map((v: SelectOption<string>) => v.value)
                );
            } else {
                fo.setFilter(fld, []);
            }
            route(`${props.publicPath}q/${fo.toString()}`);
        };

    return (
        <div>
            <div class="main-header">
                <h3>TZIP Explorer</h3>
                <Helmet>
                    {/* This thing sets the document title. */}
                    <title>TZIP Explorer</title>
                </Helmet>
            </div>
            <div class="nav-container">
                <div class="item">
                    <span>Type</span>
                    <Select
                        class="selector-dropdown-filter"
                        aria-label="filter-by"
                        isMulti
                        onChange={handleChange("Type")}
                        options={typeOptions}
                        value={mkSelectValues(fo.getFilters("Type"))}
                    />
                </div>
                <div class="item">
                    <span>Status</span>
                    <Select
                        class="selector-dropdown-filter"
                        aria-label="filter-by"
                        isMulti
                        onChange={handleChange("Status")}
                        options={statusOptions}
                        value={mkSelectValues(fo.getFilters("Status"))}
                    />
                </div>
                <div class="item">
                    <span>Sort By</span>
                    <Select
                        class="selector-dropdown-sort"
                        aria-label="sort-by"
                        onChange={handleSortSelection}
                        options={sortOptions}
                        value={{
                            label: fo.sortAttribute,
                            value: fo.sortAttribute
                        }}
                    />
                </div>
                <div class="item">
                    <span>Show Drafts</span>
                    <input
                        type="checkbox"
                        class="select-drafts-checkbox"
                        checked={fo.showDrafts}
                        onClick={toggleShowDrafts}
                    />
                </div>
            </div>
            <Tzips
                publicPath={props.publicPath}
                selector={fo}
                repoUrl={props.indexData.repoUrl}
                tzips={props.indexData.bubbles}
                draftTzips={
                    fo.showDrafts ? props.indexData.draftBubbles : new Map()
                }
            />
        </div>
    );
};

export default Home;

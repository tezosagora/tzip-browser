// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { getFiles, setupPrecaching, setupRouting } from "preact-cli/sw/";

setupRouting();
setupPrecaching(getFiles());

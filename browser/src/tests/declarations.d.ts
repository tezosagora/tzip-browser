// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// Enable enzyme adapter's integration with TypeScript
// See: https://github.com/preactjs/enzyme-adapter-preact-pure#usage-with-typescript
/// <reference types="enzyme-adapter-preact-pure" />

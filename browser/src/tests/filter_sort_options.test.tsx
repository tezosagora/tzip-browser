// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later
import { FilterSortOptions, SafeHtml, TzipMeta, TzipBubbles } from "../common";
import { buildTzips } from "../json";

const indexData = buildTzips({
    "repo-url": "http://test",
    "draft-bubbles": {},
    bubbles: {
        "tzip-10": {
            meta: {
                status: "Final",
                created: "2019-04-10",
                author: "Bob",
                title: "TZIP Purpose and Guidelines",
                type: "Meta"
            }
        },
        "tzip-1": {
            meta: {
                status: "Draft",
                created: "2019-09-17",
                author: "John",
                title: "Wallet Interaction Standard",
                type: "LA"
            }
        },
        "tzip-13": {
            meta: {
                status: "Draft",
                created: "2020-01-24",
                author: "Charlie",
                title: "FA2 - Multi-Asset Interface",
                type: "Financial Application (FA)"
            }
        },
        "tzip-12": {
            meta: {
                status: "Work In Progress",
                created: "2020-01-02",
                author: "Bob",
                title: "FA1.3 - Fungible Asset",
                type: "Financial Application"
            }
        },
        "tzip-16": {
            meta: {
                status: "Draft",
                created: "2020-04-01",
                author: "Aaron",
                title: "GraphQL interface to Tezos node data - Scalars",
                type: "Interface"
            }
        },
        "tzip-15": {
            meta: {
                status: "Draft",
                created: "2020-05-14",
                author: "Michael",
                title: "A2 - Token Transferlist Interface",
                type: "Application (A)"
            }
        },
        "tzip-14": {
            meta: {
                status: "Work In Progress",
                created: "2020-06-30",
                author: "Brandon",
                title: "Contract Metadata",
                type: "Interface"
            }
        },
        "tzip-18": {
            meta: {
                status: "Work In Progress",
                created: "2020-08-17",
                author: "Aaron",
                title: "A2 - Upgradeable Contracts",
                type: "Application (A)"
            }
        },
        "tzip-2": {
            meta: {
                status: "Final",
                created: "2019-04-10",
                author: "John",
                title: "TZIP Types and Naming",
                type: "Meta"
            }
        },
        "tzip-20": {
            meta: {
                status: "Work In Progress",
                created: "2020-10-07",
                author: "Michael",
                title: "Off-chain Events",
                type: "Interface"
            }
        },
        "tzip-3": {
            meta: {
                status: "Final",
                created: "2019-04-10",
                author: "John",
                title: "TZIP Code of Conduct",
                type: "Meta"
            }
        },
        "tzip-4": {
            meta: {
                status: "Deprecated",
                created: "2019-04-11",
                author: "Charlie",
                title: "A1 - Michelson Contract Interfaces and Conventions",
                type: "Application"
            }
        },
        "tzip-5": {
            meta: {
                status: "Deprecated",
                created: "2019-04-12",
                author: "Ivan",
                title: "FA1 - Abstract Ledger",
                type: "Financial Application"
            }
        },
        "tzip-6": {
            meta: {
                status: "Deprecated",
                created: "2019-05-04",
                author: "John",
                title: "A1.1 - Balanced Trees for nested or and pair types",
                type: "Application"
            }
        },
        "tzip-7": {
            meta: {
                status: "Final",
                created: "2019-06-20",
                author: "Kostya",
                title: "FA1.2 - Approvable Ledger",
                type: "Financial Application"
            }
        },
        "tzip-8": {
            meta: {
                status: "Work In Progress",
                created: "2019-06-25",
                author: "Martin",
                title: "Payment Request Format",
                type: "Interface"
            }
        },
        "tzip-9": {
            meta: {
                status: "Work In Progress",
                created: "2019-06-25",
                author: "Martin",
                title: "Info Field for Payment Requests",
                type: "Informational"
            }
        }
    }
});

const bubbles = indexData.bubbles;

// We only need to extract proposal id to check result, but we also return an
// attribute to manualy check the result if need arises
function extractAttrWithTzip(
    tzips: TzipBubbles,
    key: Exclude<keyof TzipMeta, "discourseTopicId"> | "proposalID"
): Array<[string, string]> | Array<string> {
    if (key === "proposalID") {
        const ret: Array<string> = [];
        tzips.forEach((v, k: string) => {
            ret.push(k);
        });
        return ret;
    } else {
        const ret: Array<[string, string]> = [];
        tzips.forEach((v, k: string) => {
            const x = v.meta[key];
            if (x !== undefined) {
                if (x instanceof SafeHtml) {
                    ret.push([k, x.__html]);
                } else ret.push([k, x]);
            }
        });
        return ret;
    }
}

test("Sorts bubbles by date", () => {
    const fo = FilterSortOptions.from("sort=Date");
    const expected = Array.from([
        ["tzip-10", "2019-04-10"],
        ["tzip-2", "2019-04-10"],
        ["tzip-3", "2019-04-10"],
        ["tzip-4", "2019-04-11"],
        ["tzip-5", "2019-04-12"],
        ["tzip-6", "2019-05-04"],
        ["tzip-7", "2019-06-20"],
        ["tzip-8", "2019-06-25"],
        ["tzip-9", "2019-06-25"],
        ["tzip-1", "2019-09-17"],
        ["tzip-12", "2020-01-02"],
        ["tzip-13", "2020-01-24"],
        ["tzip-16", "2020-04-01"],
        ["tzip-15", "2020-05-14"],
        ["tzip-14", "2020-06-30"],
        ["tzip-18", "2020-08-17"],
        ["tzip-20", "2020-10-07"]
    ]);
    const actual = extractAttrWithTzip(fo.filterSort(bubbles), "created");
    expect(actual).toStrictEqual(expected);
});

test("Sorts bubbles by Proposal ID", () => {
    const fo = FilterSortOptions.from("sort=Proposal ID");
    const expected = Array.from([
        "tzip-1",
        "tzip-2",
        "tzip-3",
        "tzip-4",
        "tzip-5",
        "tzip-6",
        "tzip-7",
        "tzip-8",
        "tzip-9",
        "tzip-10",
        "tzip-12",
        "tzip-13",
        "tzip-14",
        "tzip-15",
        "tzip-16",
        "tzip-18",
        "tzip-20"
    ]);
    const actual = extractAttrWithTzip(fo.filterSort(bubbles), "proposalID");
    expect(actual).toStrictEqual(expected);
});

test("Sorts bubbles by Title", () => {
    const fo = FilterSortOptions.from("sort=Title");
    const expected = Array.from([
        ["tzip-4", "A1 - Michelson Contract Interfaces and Conventions"],
        ["tzip-6", "A1.1 - Balanced Trees for nested or and pair types"],
        ["tzip-15", "A2 - Token Transferlist Interface"],
        ["tzip-18", "A2 - Upgradeable Contracts"],
        ["tzip-14", "Contract Metadata"],
        ["tzip-5", "FA1 - Abstract Ledger"],
        ["tzip-7", "FA1.2 - Approvable Ledger"],
        ["tzip-12", "FA1.3 - Fungible Asset"],
        ["tzip-13", "FA2 - Multi-Asset Interface"],
        ["tzip-16", "GraphQL interface to Tezos node data - Scalars"],
        ["tzip-9", "Info Field for Payment Requests"],
        ["tzip-20", "Off-chain Events"],
        ["tzip-8", "Payment Request Format"],
        ["tzip-3", "TZIP Code of Conduct"],
        ["tzip-10", "TZIP Purpose and Guidelines"],
        ["tzip-2", "TZIP Types and Naming"],
        ["tzip-1", "Wallet Interaction Standard"]
    ]);
    const actual = extractAttrWithTzip(fo.filterSort(bubbles), "title");
    expect(actual).toStrictEqual(expected);
});

test("Filters bubbles by Author", () => {
    const fo = FilterSortOptions.from("Author=John");
    const expected = Array.from([
        ["tzip-2", "John"],
        ["tzip-3", "John"],
        ["tzip-6", "John"],
        ["tzip-1", "John"]
    ]);

    const actual = extractAttrWithTzip(fo.filterSort(bubbles), "author");
    expect(actual).toStrictEqual(expected);
});

test("Filters bubbles by Status", () => {
    const fo = FilterSortOptions.from("Status=Draft");
    const expected = Array.from([
        ["tzip-1", "Draft"],
        ["tzip-13", "Draft"],
        ["tzip-16", "Draft"],
        ["tzip-15", "Draft"]
    ]);

    const actual = extractAttrWithTzip(fo.filterSort(bubbles), "status");
    expect(actual).toStrictEqual(expected);
});

test("Filters bubbles by Type", () => {
    const fo = FilterSortOptions.from("Type=Meta");
    const expected = Array.from([
        ["tzip-10", "Meta"],
        ["tzip-2", "Meta"],
        ["tzip-3", "Meta"]
    ]);

    const actual = extractAttrWithTzip(fo.filterSort(bubbles), "type");
    expect(actual).toStrictEqual(expected);
});

test("Filters bubbles by Author and sort by proposal Id", () => {
    const fo = FilterSortOptions.from("Author=John&sort=Proposal ID");
    const expected = Array.from([
        ["tzip-1", "John"],
        ["tzip-2", "John"],
        ["tzip-3", "John"],
        ["tzip-6", "John"]
    ]);

    const actual = extractAttrWithTzip(fo.filterSort(bubbles), "author");
    expect(actual).toStrictEqual(expected);
});

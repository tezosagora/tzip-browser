// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { FunctionalComponent, h } from "preact";
import { route } from "preact-router";
import RenderedContainer from "../rendered_container";
import { DraftTzipBubble, TzipBubble } from "../../common";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCode } from "@fortawesome/free-solid-svg-icons/faCode";

export interface Props {
    publicPath: string;
    tzipId: string;
    tzipBubble: TzipBubble | DraftTzipBubble;
    repoUrl: string;
}

function convertStatus(st: string | undefined): string | undefined {
    if (st && st == "Work In Progress") return "WIP";
    else return st;
}

function getStatusColor(st: string | undefined): string {
    switch (convertStatus(st)) {
        case "Final": {
            return "status-final";
        }
        case "Deprecated": {
            return "status-deprecated";
        }
        case "WIP": {
            return "status-wip";
        }
        default: {
            return "status-default";
        }
    }
}

export const TzipBubbleComp: FunctionalComponent<Props> = (props: Props) => {
    if (props.tzipBubble instanceof TzipBubble) {
        const bubble = props.tzipBubble;
        return (
            <div class="tzip-container">
                <div
                    class="tzip-clickable"
                    onClick={(): void => {
                        route(`${props.publicPath}proposal/${props.tzipId}/`);
                    }}
                >
                    <div class="tzip-id">{props.tzipId}</div>
                    <RenderedContainer
                        class="tzip-title"
                        innerHtml={bubble.meta.title}
                    />
                    <RenderedContainer
                        class="tzip-intro"
                        innerHtml={bubble.summary}
                    />
                </div>
                <div class="tzip-status-and-link">
                    <div
                        class={
                            "tzip-status " + getStatusColor(bubble.meta?.status)
                        }
                    >
                        {convertStatus(bubble.meta.status)}
                    </div>

                    <div class="tzip-repo-link">
                        <a
                            rel="noreferrer"
                            target="_blank"
                            href={bubble.directLink}
                        >
                            <FontAwesomeIcon icon={faCode} color="#444" />
                        </a>
                    </div>
                </div>
            </div>
        );
    } else {
        const bubble = props.tzipBubble;
        return (
            <div class="draft-tzip-container">
                <div
                    class="tzip-clickable"
                    onClick={(): void => {
                        route(`${props.publicPath}draft/${props.tzipId}/`);
                    }}
                >
                    <div class="tzip-id">{props.tzipId}</div>

                    <RenderedContainer
                        class="tzip-title"
                        innerHtml={bubble.meta.title}
                    />
                    <RenderedContainer
                        class="tzip-intro"
                        innerHtml={bubble.summary}
                    />
                </div>
                <div class="tzip-status-and-link">
                    <div class="tzip-repo-link">
                        <a
                            rel="noreferrer"
                            target="_blank"
                            href={bubble.directLink}
                        >
                            <FontAwesomeIcon icon={faCode} color="#444" />
                        </a>
                    </div>
                </div>
            </div>
        );
    }
};

export default TzipBubbleComp;

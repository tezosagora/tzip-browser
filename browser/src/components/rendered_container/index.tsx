// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { FunctionalComponent, h } from "preact";
import { SafeHtml } from "../../common";

interface Props {
    innerHtml: SafeHtml | undefined;
    class: string;
}

const RenderedContainer: FunctionalComponent<Props> = (props: Props) => {
    if (props.innerHtml !== undefined) {
        if (typeof window === "undefined") {
            // We are doing SSR, so this works, apparently.
            return <div>{props.innerHtml.__html}</div>;
        } else {
            // We could be in browser, so use the usual method with dengerouslySetInnerHTML.
            return (
                <div
                    class={props.class}
                    dangerouslySetInnerHTML={props.innerHtml}
                ></div>
            );
        }
    }
    return <span />;
};

export default RenderedContainer;

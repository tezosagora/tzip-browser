// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { FunctionalComponent, h } from "preact";
import { TzipBubble, ProposalId } from "../../common";
import { Link } from "preact-router/match";

interface Props {
    tzips: Map<ProposalId, TzipBubble>;
}

function collectTzipTypes(tzips: Map<ProposalId, TzipBubble>): Array<string> {
    const ret: Map<string, boolean> = new Map();

    tzips.forEach(val => {
        if (val?.meta?.type) {
            ret.set(val.meta.type, true);
        }
    });

    return Array.from(ret.keys());
}

const TzipNav: FunctionalComponent<Props> = (props: Props) => {
    const tzipTypes = collectTzipTypes(props.tzips);

    const navItems: Array<h.JSX.Element> = [];

    tzipTypes.forEach(function(val) {
        navItems.push(
            <Link activeClassName="link-active" href={"/" + val}>
                {val}
            </Link>
        );
    });

    return (
        <nav>
            <div class="nav-links">{navItems}</div>
        </nav>
    );
};

export default TzipNav;

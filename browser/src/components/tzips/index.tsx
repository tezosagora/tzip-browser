// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { Fragment, FunctionalComponent, h } from "preact";
import { DraftTzipBubbles, TzipBubbles, FilterSortOptions } from "../../common";
import TzipBubbleComp from "../../components/tzipBubble";

interface Props {
    repoUrl: string;
    publicPath: string;
    selector: FilterSortOptions;
    tzips: TzipBubbles;
    draftTzips: DraftTzipBubbles;
}

const Tzips: FunctionalComponent<Props> = (props: Props) => {
    const tzipComps: Array<h.JSX.Element> = [];
    const draftTzipComps: Array<h.JSX.Element> = [];

    const filteredSortedTzip: TzipBubbles = props.selector.filterSort(
        props.tzips
    );

    filteredSortedTzip.forEach((val, key) => {
        tzipComps.push(
            <TzipBubbleComp
                publicPath={props.publicPath}
                repoUrl={props.repoUrl}
                tzipBubble={val}
                tzipId={key}
            />
        );
    });

    props.draftTzips.forEach((val, key) => {
        draftTzipComps.push(
            <TzipBubbleComp
                publicPath={props.publicPath}
                repoUrl={props.repoUrl}
                tzipBubble={val}
                tzipId={key}
            />
        );
    });

    function TzipDrafts() {
        if (props.draftTzips.size > 0) {
            return (
                <Fragment>
                    <div class="draft-header">TZIP Drafts</div>
                    <div class="draft-tzips-container">{draftTzipComps}</div>
                </Fragment>
            );
        } else {
            return <Fragment />;
        }
    }

    return (
        <div>
            <div class="tzips-container">{tzipComps}</div>
            <TzipDrafts />
        </div>
    );
};

export default Tzips;

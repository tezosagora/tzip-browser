// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

module.exports = {
    webpackFinal: async (config) => {
        // Typescript support
        config.module.rules.push({
            test: /\.(ts|tsx)$/,
            use: [{
                loader: require.resolve('ts-loader'),
            }],
        });
        config.resolve.extensions.push('.ts', '.tsx');

        // // Alias for react (needed to use react component)
        const aliases = config.resolve.alias || (config.resolve.alias = {});
        aliases["react"] = "preact/compat";
        aliases["react-dom"] = "preact/compat";
        aliases["react-dom/test-utils"] = "preact/test-utils";

        return config;
    }
};
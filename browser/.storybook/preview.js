// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import '../src/style/index.css';
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}

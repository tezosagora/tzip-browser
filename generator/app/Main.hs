-- SPDX-FileCopyrightText: 2021 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later
module Main
  ( main
  ) where

import Options.Generic (getRecord, unHelpful)
import Universum

import Cmd
import Generator

main :: IO ()
main = do
  getRecord  "TZIP Generator" >>= \case
    GenerateCommand (unHelpful -> path) (unHelpful -> target) (unHelpful -> mrepoUrl) ->
      generate path target $ fromMaybe "https://gitlab.com/tezos/tzip/-/blob/master" mrepoUrl

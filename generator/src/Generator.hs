-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-License-Identifier: AGPL-3.0-or-later
module Generator
  ( generate
  ) where

import Universum

import Data.Aeson (encode)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Map as M
import Streamly (asyncly)
import System.Directory (createDirectoryIfMissing, doesDirectoryExist)
import Shelly (cp_r, rm_rf, shelly)
import System.FilePath ((</>), (<.>), takeExtension)
import qualified Streamly.Prelude as S
import Streamly.Internal.FileSystem.Dir as S
import System.IO

import Common
import Tzip

-- | Given a source repo path and a destination
-- path, write the index proposal file and data
-- files for each available proposal.
generate :: FilePath -> FilePath -> Text -> IO ()
generate fp target repoUrl = do
  -- Set line buffering so that logs from multiple threads
  -- are not interleaved.
  hSetBuffering stderr LineBuffering
  -- In the Tzip repo, proposals are in the "proposals" directory. So
  -- we set to read the files from the same.
  let srcDir = fp </> "proposals"
  let draftsSrcDir = fp </> "drafts/current"

  createDirectoryIfMissing True proposalsDir
  createDirectoryIfMissing True draftsDir

  let
    runPipeline
      :: Monoid c
      => FilePath -- The source folder
      -> (FilePath -> Either FilePath FilePath -> Maybe FilePath)
          -- A callback that accept a source path and a relative path that is either
          -- a directory or a file, and may be return the path to a proposal markdown
          -- document.
      -> (c -> b -> IO c)
          -- A callback that aggregates all proposals into an accumilator
      -> (Text -> (FilePath, ByteString) -> IO a)
          -- A callback that takes a repoUrl, and a filepath and a proposal text
          -- and return a parsed result
      -> (a -> IO b)
          -- A callback that the parsed result and return the final result of the inner
          -- pipeline
      -> IO c
    runPipeline srcPath mdPathFn foldFn cb1 cb2 = S.foldlM' foldFn mempty $ asyncly
      $ S.toEither srcPath
      & S.mapMaybe (mdPathFn srcPath)
      & S.mapM (\fp' -> (fp', ) <$> BS.readFile fp')
      & S.filter (\(_, b) -> BS.length b > 0)
      & S.mapM (try @_ @ProcessingException . cb1 repoUrl)
      & S.mapMaybeM (\case
          Right p -> pure $ Just p
          Left err -> do
            logMessage $ show @Text err
            pure Nothing)
      & S.mapM cb2

  tzipMap <- runPipeline srcDir getTzipMarkdownPath (tzipBubblesFoldFn srcDir) parseProposal compileProposal
  drafts <-  doesDirectoryExist draftsSrcDir >>= \case
    True -> runPipeline draftsSrcDir getDraftMarkdownPath draftTzipBubblesFoldFn parseDraft compileDraftProposal
    _ -> pure mempty

  putTextLn "Writing proposals.json"
  writeProposals (target </> "proposals" <.> "json") tzipMap drafts repoUrl
  where
    proposalsDir = target </> "proposal"
    draftsDir = proposalsDir

    proposalToFilePath :: Tzip -> FilePath
    proposalToFilePath Tzip{tzProposalId} =
      proposalsDir </> toString tzProposalId <.> "json"

    draftToFilePath :: DraftTzip -> FilePath
    draftToFilePath DraftTzip{dtzFileName} =
      draftsDir </> dtzFileName <.> "json"

    draftTzipBubblesFoldFn :: DraftTzipBubbles -> (DraftTzip, DraftTzipBubble) -> IO DraftTzipBubbles
    draftTzipBubblesFoldFn m (draft, draftBubble) = do
        BSL.writeFile (draftToFilePath draft) $ encode draft
        pure $ M.insert (dtzFileName draft) draftBubble m

    tzipBubblesFoldFn :: FilePath -> TzipBubbles -> (Tzip, TzipBubble) -> IO TzipBubbles
    tzipBubblesFoldFn srcDir m (tzip, tzbubble) = do
        BSL.writeFile (proposalToFilePath tzip) $ encode tzip
        shelly $ do
          let srcPath = srcDir </> toString (tzProposalId tzip)
              destPath = proposalsDir </> toString (tzProposalId tzip)
          rm_rf destPath
          cp_r srcPath destPath
        pure $ M.insert (tzProposalId tzip) tzbubble m

    getTzipMarkdownPath :: FilePath -> Either FilePath FilePath -> Maybe FilePath
    getTzipMarkdownPath proposals (Left tzip) = Just $ proposals </> tzip </> (tzip <.> "md")
    getTzipMarkdownPath _ _  = Nothing

    getDraftMarkdownPath :: FilePath -> Either FilePath FilePath -> Maybe FilePath
    getDraftMarkdownPath draftsFld (Right tzip) = if (takeExtension tzip) == ".md" then Just $ draftsFld </> tzip else Nothing
    getDraftMarkdownPath _ _ = Nothing

-- | Write the index proposals.json which contains metadata of all available
-- proposals
writeProposals :: FilePath -> TzipBubbles -> DraftTzipBubbles -> Text -> IO ()
writeProposals fp bubbleData draftBubbleData repoUrl = BSL.writeFile fp $ encode $ IndexProposals bubbleData draftBubbleData Nothing repoUrl

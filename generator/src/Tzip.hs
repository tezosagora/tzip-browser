-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-License-Identifier: AGPL-3.0-or-later
module Tzip
  ( IndexProposals(..)
  , ProcessingException
  , ProposalId (..)
  , DraftTzip(..)
  , DraftTzipMeta(..)
  , DraftTzipBubble(..)
  , Tzip(..)
  , TzipBubble(..)
  , DraftTzipBubbles
  , TzipBubbles
  , TzipMeta(..)
  , TzipStatus(..)
  , compileProposal
  , compileDraftProposal
  , parseProposal
  , parseDraft
  ) where

import Universum

import Data.Aeson (FromJSON(..), FromJSONKey(..), ToJSON(..), ToJSONKey(..),
                   withText)
import Data.Aeson.Types (Parser, Value(..))
import Data.Default (def)
import Data.List (findIndex)
import qualified Data.Map as M
import qualified Data.Text as T
import Data.Yaml (decodeEither')
import Deriving.Aeson (CustomJSON(..))
import System.FilePath (takeFileName, takeBaseName, takeDirectory)
import System.Timeout (timeout)
import Text.Megaparsec as MP
import Text.Megaparsec.Char (space, string')
import qualified Text.Megaparsec.Char.Lexer as L
import Text.Pandoc (Block(..), Extension(..), Inline(..), Meta(..), MetaValue(..), Pandoc(..),
  PandocError, enableExtension, readerExtensions)
import Text.Pandoc.Class (PandocIO, runIO)
import Text.Pandoc.Readers.CommonMark (readCommonMark)

import Common

data TzipStatus
  = WorkInProgress
  | Draft
  | Withdrawn
  | Submitted
  | Deprecated
  | Superseded
  | Final
  deriving stock (Eq, Ord, Show)

instance FromJSON TzipStatus where
  parseJSON = withText "TzipStatus" txtToStatus
    where
      txtToStatus :: Text -> Parser TzipStatus
      txtToStatus "Deprecated" = pure Deprecated
      txtToStatus "Draft" = pure Draft
      txtToStatus "Final" = pure Final
      txtToStatus "Submitted" = pure Submitted
      txtToStatus "Superseded" = pure Superseded
      txtToStatus "Withdrawn" = pure Withdrawn
      txtToStatus "Work In Progress" = pure WorkInProgress
      txtToStatus st = fail $ "Unknown proposal status:" <> toString st

instance ToJSON TzipStatus where
  toJSON = \case
    Deprecated -> String "Deprecated"
    Draft -> String "Draft"
    Final -> String "Final"
    Submitted -> String "Submitted"
    Superseded -> String "Superseded"
    Withdrawn -> String "Withdrawn"
    WorkInProgress -> String "Work In Progress"

data TzipMeta = TzipMeta
  { tzmStatus :: TzipStatus
  , tzmCreated :: Text -- @TODO https://gitlab.com/tezosagora/tzip-browser/-/issues/52.
  , tzmAuthor :: Text
  , tzmTitle :: SafeHtml
  , tzmType :: Text
  , tzmDiscourseTopicId :: Maybe Int
  } deriving stock (Eq, Ord, Generic, Show)
    deriving (FromJSON, ToJSON)
      via CustomDerivation "tzm" TzipMeta

newtype Version = Version Text
  deriving stock (Generic, Show)
  deriving newtype (Ord, Eq, IsString)
  deriving ToJSON
    via CustomDerivation "ver" Version

instance FromJSON Version where
  parseJSON v =
    Version <$> (parseJSON @Text v
      <|> (show <$> parseJSON @Int v)
      <|> (show <$> parseJSON @Double v))

data DraftTzipMeta = DraftTzipMeta
  { dtzmCreated :: Maybe Text -- @TODO https://gitlab.com/tezosagora/tzip-browser/-/issues/52
  , dtzmDate :: Maybe Text -- @TODO https://gitlab.com/tezosagora/tzip-browser/-/issues/52
  , dtzmAuthor :: Text
  , dtzmTitle :: SafeHtml
  , dtzmType :: Text
  , dtzmVersion :: Maybe Version
  } deriving stock (Eq, Ord, Generic, Show)
    deriving (FromJSON, ToJSON)
      via CustomDerivation "dtzm" DraftTzipMeta

data TzipBubble = TzipBubble
  { tzbMeta :: TzipMeta
  , tzbSummary :: SafeHtml
  , tzbDirectLink :: Text
  } deriving stock (Eq, Ord, Generic, Show)
    deriving (FromJSON, ToJSON)
      via CustomDerivation "tzb" TzipBubble

data DraftTzipBubble = DraftTzipBubble
  { dtzbMeta :: DraftTzipMeta
  , dtzbSummary :: SafeHtml
  , dtzbDirectLink :: Text
  } deriving stock (Eq, Ord, Generic, Show)
    deriving (FromJSON, ToJSON)
      via CustomDerivation "dtzb" DraftTzipBubble

data Tzip = Tzip
  { tzProposalId :: ProposalId
  , tzSummary :: SafeHtml
  , tzMeta :: TzipMeta
  , tzSource :: Text
  , tzRendered :: SafeHtml
  , tzDirectLink :: Text
  } deriving stock (Generic, Show)
    deriving ToJSON
      via CustomDerivation "tz" Tzip

data DraftTzip = DraftTzip
  { dtzSummary :: SafeHtml
  , dtzMeta :: DraftTzipMeta
  , dtzSource :: Text
  , dtzRendered :: SafeHtml
  , dtzDirectLink :: Text
  , dtzFileName :: FilePath
  } deriving stock (Generic, Show)
    deriving ToJSON
      via CustomDerivation "dtz" DraftTzip

newtype ProposalId = ProposalId { unProposalId :: Text }
  deriving stock (Show, Generic, Ord, Eq)
  deriving newtype (ToJSON, ToString)

deriving newtype instance ToJSONKey ProposalId
deriving newtype instance FromJSONKey ProposalId

-- We have custom instance for FromJSON so that we can
-- read it from numeric literals, as found in yaml front matter.
-- If left for derived instance, it will error out because the
-- Text parser expects a `String` (constructor of Value) instead
-- of a `Number`.
instance FromJSON ProposalId where
  parseJSON (Number n) = pure $ ProposalId (show $ round @_ @Int n)
  parseJSON v = ProposalId <$> parseJSON v

type TzipBubbles = Map ProposalId TzipBubble

type DraftTzipBubbles = Map FilePath DraftTzipBubble

data IndexProposals = IndexProposals
  { ipBubbles :: TzipBubbles
  , ipDraftBubbles :: DraftTzipBubbles
  , ipVersion :: Maybe Text
  , ipRepoUrl :: Text
  } deriving stock (Generic, Show)
    deriving (FromJSON, ToJSON)
      via CustomDerivation "ip" IndexProposals

data ProcessingException
  = ParseException Text
  | CompileException Text
  | TimeoutException
  | ProposalException ProposalId ProcessingException
  | DraftProposalException FilePath ProcessingException
  deriving stock Show
instance Exception ProcessingException

-- | Parse proposal raw data and return parsed Metadata, the Pandoc document
-- and the markdown source (without frontmatter) from the proposal.
parseProposal
  :: Text
  -> (FilePath, ByteString)
  -> IO (ProposalId, TzipBubble, Pandoc, Text)
parseProposal repoUrl i@(fp, _) = parseProposal' i callback (onError proposalId)
  where
    callback :: TzipMeta -> Pandoc -> SafeHtml -> FilePath -> Text -> IO (ProposalId, TzipBubble, Pandoc, Text)
    callback meta doc summary _ mdText = do
      pure (proposalId, TzipBubble meta summary (mkDirectLink repoUrl proposalId), doc, mdText)

    onError :: ProposalId -> FilePath -> ProcessingException -> IO b
    onError proposalId_ _ pe = throwM (ProposalException proposalId_ pe)

    proposalId = mkProposalId fp

    mkProposalId :: FilePath -> ProposalId
    mkProposalId fp_ = ProposalId (toText $ takeBaseName $ takeDirectory fp_)

parseDraft
  :: Text
  -> (FilePath, ByteString)
  -> IO (FilePath, DraftTzipBubble, Pandoc, Text)
parseDraft repoUrl i@(_, _) = parseProposal' i callback onError
  where
    callback :: DraftTzipMeta -> Pandoc -> SafeHtml -> FilePath -> Text -> IO (FilePath, DraftTzipBubble, Pandoc, Text)
    callback meta doc summary fp mdText = do
      pure (fp, DraftTzipBubble meta summary (mkDirectLinkToDraft repoUrl fp), doc, mdText)

    onError :: FilePath -> ProcessingException -> IO b
    onError fp pe = throwM (DraftProposalException fp pe)

parseProposal'
  :: forall meta b. FromJSON meta
  => (FilePath, ByteString)
  -> (meta -> Pandoc -> SafeHtml -> FilePath -> Text -> IO b)
  -> (FilePath -> ProcessingException -> IO b)
  -> IO b
parseProposal' (fp, bs) docProcessor onError = flip Universum.catch handler do
  -- parse YAML out of the content
  (meta, mdText) <- splitFrontmatter @meta (decodeUtf8 bs)
  -- parse into markdown pandoc document
  runIOWithTimeout (parseDocAndGetSummary mdText) >>= \case
    Nothing -> onError fp $ TimeoutException
    Just result -> case result of
      Right (doc, summary) -> docProcessor meta doc summary fp mdText
      Left err -> onError fp $ ParseException (show err)
  where
    handler :: ProcessingException -> IO b
    handler pe = onError fp pe
    -- Parse the document, get the summary block, and render it
    -- as text to the metadata summary field.
    parseDocAndGetSummary :: Text -> PandocIO (Pandoc, SafeHtml)
    parseDocAndGetSummary mdText = do
      let readerOptionsWithExtensions = def
            { readerExtensions =
                enableExtension Ext_raw_html $
                enableExtension Ext_pipe_tables $
                enableExtension Ext_auto_identifiers $
                enableExtension Ext_gfm_auto_identifiers $
                readerExtensions def
            }
      doc@(Pandoc _ blks)
          <- readCommonMark readerOptionsWithExtensions mdText
      -- Try to find summary header, and the following paragrah.
      -- Or if not found, just use the first non empty paragraph.
      let summaryBlk = take 1 $ catMaybes  [extractSummary blks, firstNonEmptyParagrah blks]
      summary <- renderDoc $ Pandoc mempty summaryBlk
      pure (applyFilter filterFixLinks doc, summary)

    filterFixLinks :: Block -> Block
    filterFixLinks = transformInlines fixLink

applyFilter :: (Block -> Block) -> Pandoc -> Pandoc
applyFilter fn (Pandoc a blks) = Pandoc a (fn <$> blks)

-- Convert links of form (tzip-<x>/tzip-<x>.md with different kinds of path
-- leading to them) to ../tzip-<x>/
fixLink :: Inline -> Inline
fixLink inline = case inline of
  Link a i (l, t) -> Link a i (fixLink' l, t)
  a -> a
  where
    fixLink' :: Text -> Text
    fixLink' lnk = case parseMaybe parser lnk of
      Just tzipIdWithHash -> "../" <> tzipIdWithHash
      Nothing -> lnk

    parser :: Parsec Void Text Text
    parser = do
      tzip <- skipManyTill (satisfy (const True)) tzipLink
      suffix <- MP.many (satisfy (const True))
      pure $ tzip <> "/" <> toText suffix

    tzipLink :: Parsec Void Text Text
    tzipLink = do
      string' "tzip-"
      x <- intParser
      string' "/"
      string' ("tzip-" <> show x)
      string' ".md"
      pure $ "tzip-" <> (show x)

    intParser :: Parsec Void Text Int
    intParser = L.lexeme space L.decimal

-- Apply the given function on all inline elements
-- in the block.
transformInlines :: (Inline -> Inline) -> Block -> Block
transformInlines fn blk = case blk of
  Plain inlines -> Plain (fmap fn inlines)
  Para inlines -> Para (fmap fn inlines)
  BlockQuote blks -> BlockQuote $ transformBlocks blks
  OrderedList la nstblks -> OrderedList la (transformBlocks <$> nstblks)
  BulletList nstblks -> BulletList (transformBlocks <$> nstblks)
  DefinitionList lsts -> DefinitionList (transformDLInner <$> lsts)
  Header i a inlines -> Header i a (fn <$> inlines)
  Div a blks -> Div a (transformBlocks blks)
  a -> a
  where
    transformBlocks :: [Block] -> [Block]
    transformBlocks = fmap (transformInlines fn)

    transformDLInner :: ([Inline], [[Block]]) -> ([Inline], [[Block]])
    transformDLInner (inlines, nstblks) = (fn <$> inlines, transformBlocks <$> nstblks)

-- | Splits the markdown source into its frontmatter and actual markdown.
-- Only works if the input starts with the '---' prefix.
splitFrontmatter :: (FromJSON a, MonadThrow m) => Text -> m (a, Text)
splitFrontmatter (T.strip -> inp) = let
  fmMarker = T.take 3 inp
  in if fmMarker == sep
    then let
      -- drop the `sep` prefix and break the rest of doc on the next `sep`.
      (fm, doc) = T.breakOn sepTerminator (T.drop sepLength inp)
      in case decodeEither' (encodeUtf8 fm) of
        Right a -> pure (a, T.drop sepTerminatorLength doc)
        Left err -> throwM $ ParseException ("Error during frontmatter parse:" <> (show err))
    else throwM $ ParseException "Frontmatter for proposal was not found"
  where
    sep = "---"
    sepTerminator = "\n" <> sep <> "\n"
    sepLength = T.length sep
    sepTerminatorLength = T.length sepTerminator

-- | Find the first header that say "Summary" and extracts the first
-- paragraph that comes after it and return it as a Pandoc Block
extractSummary :: [Block] -> Maybe Block
extractSummary blks = do
    idx <- findIndex isSummaryHeader blks
    firstNonEmptyParagrah $ drop idx blks
  where
    isSummaryHeader :: Block -> Bool
    isSummaryHeader (Header _ _ [Str i]) = i == "Summary"
    isSummaryHeader _ = False

firstNonEmptyParagrah :: [Block] -> Maybe Block
firstNonEmptyParagrah [] = Nothing
firstNonEmptyParagrah (h:rst) = case h of
  Para (_:_) -> Just h
  Para [] -> firstNonEmptyParagrah rst
  _ -> firstNonEmptyParagrah rst

-- We return `TzipBubble` from this function so that we don't have to build it
-- again further down the processing pipeline.
compileProposal :: (ProposalId, TzipBubble, Pandoc, Text) -> IO (Tzip, TzipBubble)
compileProposal (proposalId, tzBubble, Pandoc (Meta mt) blks, mdText) =
  -- Needed this since rendering toc without title causes a lot of warnings
  let docWithTitle = Pandoc (Meta $ M.insert "title"
          (MetaString $ getSafeHtml $ tzmTitle $ tzbMeta tzBubble) mt) blks
      renderFunc = case doesTocExist docWithTitle of
        True -> renderDoc
        False -> renderDocWithToc
  -- render it into html and store in Tzip type.
  in runIOWithTimeout (renderFunc docWithTitle) >>= \case
    Nothing -> throwM $ ProposalException proposalId TimeoutException
    Just result -> case result of
      Right rTxt -> do
        pure $ (Tzip
          { tzProposalId = proposalId
          , tzMeta = tzbMeta tzBubble
          , tzSummary = tzbSummary tzBubble
          , tzSource = mdText
          , tzRendered = rTxt
          , tzDirectLink = tzbDirectLink tzBubble
          }, tzBubble)
      Left err -> throwM $ CompileException (show err)

compileDraftProposal :: (FilePath, DraftTzipBubble, Pandoc, Text) -> IO (DraftTzip, DraftTzipBubble)
compileDraftProposal (fp, draftBubble, Pandoc (Meta mt) blks, mdText) =
  -- Needed this since rendering toc without title causes a lot of warnings
  let docWithTitle = Pandoc (Meta $ M.insert "title"
          (MetaString $ getSafeHtml $ dtzmTitle $ dtzbMeta draftBubble) mt) blks
      renderFunc = case doesTocExist docWithTitle of
        True -> renderDoc
        False -> renderDocWithToc
  -- render it into html and store in Tzip type.
  in runIOWithTimeout (renderFunc docWithTitle) >>= \case
    Nothing -> throwM $ DraftProposalException fp TimeoutException
    Just result -> case result of
      Right rTxt -> do
        pure $ (DraftTzip
          { dtzMeta = dtzbMeta draftBubble
          , dtzSummary = dtzbSummary draftBubble
          , dtzSource = mdText
          , dtzRendered = rTxt
          , dtzDirectLink = dtzbDirectLink draftBubble
          , dtzFileName = takeFileName fp
          }, draftBubble)
      Left err -> throwM $ CompileException (show err)

mkDirectLink :: Text -> ProposalId -> Text
mkDirectLink repoUrl ((toText . toString) -> proposalId) =
  repoUrl <> "/proposals/" <> proposalId <> "/" <> proposalId <> ".md"

mkDirectLinkToDraft :: Text -> FilePath -> Text
mkDirectLinkToDraft repoUrl fp =
  repoUrl <>"/drafts/current/" <> (toText (takeFileName fp))

runIOWithTimeout :: PandocIO a -> IO (Maybe (Either PandocError a))
runIOWithTimeout pandocOp =
  timeout (10 * 1000000) (runIO pandocOp)

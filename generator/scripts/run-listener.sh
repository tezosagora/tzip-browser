#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2021 Tezos Commons
# SPDX-License-Identifier: AGPL-3.0-or-later
export TZIP_LISTENER_GITLAB_TOKEN=secret
export TZIP_LISTENER_SOURCE_DIR=listener-source
export TZIP_LISTENER_OUTPUT_DIR=listener-output
export TZIP_LISTENER_LISTEN_UNIX_SOCKET=False
export TZIP_LISTENER_PORT=8082
export TZIP_LISTENER_REPO_LINK=https://gitlab.com/rinn7e/tzip # without `.git` extension
stack exec -- tzip-listener

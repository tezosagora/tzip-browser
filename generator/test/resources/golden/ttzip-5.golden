<h2>Table of contents</h2>

<ul>
<li><a href="#summary">Summary</a></li>
<li><a href="#abstract">Abstract</a></li>
<li><a href="#rationale">Rationale</a></li>
<li><a href="#ttzip-workflow">TTZIP workflow</a></li>
<li><a href="#some-raw-html">Some raw html</a></li>
</ul>
<h2 id="summary">Summary</h2>
<p>This is a TZIP-like document meant to use for <em>testing purposes</em>. It doesn’t containt any useful information, just some structure that resembles that of actual TZIPs.</p>
<h2 id="abstract">Abstract</h2>
<p>Tezos is a blockchain platform and, since it is meant to be used by a large number of people, it needs standards. A <a href="../tzip-1/">TZIP</a> is a Tezos standard. There is a <a href="https://gitlab.com/tzip/tzip/">whole repository</a> of such standards. It is possible that tools will be created for working with these standards (for example, presenting them in a nice way), and therefore it is useful to have test data to feed to those tools for testing. This file is one such example.</p>
<h2 id="rationale">Rationale</h2>
<p>First of all, TZIPs are Markdown files that contain information. But they are not just any Markdown files – TZIPs have to be structured in a special way, which makes them more uniform and easier to navigate and use. <a href="../tzip-1/">TZIP-1</a> is a TZIP that specified the structure of all TZIPs.</p>
<p>Humans like developing tools that make their lives easier. In particular, it is easy to imagine someone creating a tool that will process TZIPs in some way, for example, to make navigation easier – it can allow one to sort and filter TZIPs based on their metadata or it can allow full-text search in their titles and bodies.</p>
<p>Such a tool would need to be tested. It is often convenient to have small test inputs, rather than using actual real data, which tends to be bigger than needed for most test cases. Hence TTZIPs – a collection of test data that resembles TZIPs, but is not too large in size.</p>
<h2 id="ttzip-workflow">TTZIP workflow</h2>
<p>Just add new files next to this one. Feel free to fill the files with all kinds of syntax and weird things, so that they can be used in test cases.</p>
<h2 id="some-raw-html">Some raw html</h2>
<table>
  <tr>
    <th>RPC schema</th>
    <th>Request schema</th>
  </tr>
  <tr>
    <td>
      <pre>{
    &quot;kind&quot;: &quot;transaction&quot;,
    &quot;amount&quot;: $mutez,
    &quot;destination&quot;: $contract_id,
    &quot;source&quot;: $contract_id,
    &quot;counter&quot;: $positive_bignum,
    &quot;fee&quot;: $mutez,
    &quot;gas_limit&quot;: $positive_bignum,
    &quot;storage_limit&quot;: $positive_bignum,
    &quot;parameters&quot;?: $micheline.michelson_v1.expression
}</pre>
    </td>
    <td>
      <pre>{
    &quot;kind&quot;: &quot;transaction&quot;,
    &quot;amount&quot;: $mutez,
    &quot;destination&quot;: $contract_id,
    &quot;fee&quot;?: $mutez,
    &quot;gas_limit&quot;?: $positive_bignum,
    &quot;storage_limit&quot;?: $positive_bignum,
    &quot;parameters&quot;?: $micheline.michelson_v1.expression
}</pre>
    </td>
  </tr>
</table>
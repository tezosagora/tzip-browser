-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-License-Identifier: AGPL-3.0-or-later
module Integration
  ( test_FileGeneration
  ) where

import           Universum

import           Data.Aeson
import           Control.Monad              (liftM)
import qualified Data.Map                   as M
import qualified Data.Set                   as S
import           System.Directory           (createDirectoryIfMissing,
                                             doesDirectoryExist, listDirectory)
import           System.FilePath            (makeRelative, (<.>), (</>))
import           System.IO.Temp             (withSystemTempDirectory)

import           Shelly                     (shelly, touchfile)
import           Test.Tasty                 (TestTree)
import           Test.Tasty.Golden.Advanced (goldenTest)

import           Common
import           Generator
import           Tzip

testRepoPath :: FilePath
testRepoPath = "test/resources/repo"

testOutputTemplate :: FilePath
testOutputTemplate = "tzip-web-generator-test"

testRepoUrl :: Text
testRepoUrl = "http://test-repo-url"

test_FileGeneration :: [TestTree]
test_FileGeneration =
  let
    expectedGeneratedFiles =
      [ "proposal/tzip-1.json"
      , "proposal/tzip-1/tzip-1.md"
      , "proposal/tzip-1/tzip-workflow.png"

      , "proposal/tzip-12.json"
      , "proposal/tzip-12/tzip-12.md"
      , "proposal/a-draft-proposal.md.json"


      , "proposal/tzip-13.json"
      , "proposal/tzip-13/tzip-13.md"
      , "proposal/tzip-13/tzip-workflow.png"

      , "proposals.json"
      ]
    expectedBubbleData =
      [ (ProposalId "tzip-1", TzipBubble
        { tzbSummary = mkSafeHtml $
            "<p>TZIP (pronounce \"tee-zip\") stands for Tezos Interoperability Proposal, " <>
            "which are documents that explain how the Tezos blockchain can be " <>
            "improved with new and updated standards and concepts, such as smart " <>
            "contract specifications.</p>"
        , tzbMeta = TzipMeta
          { tzmStatus = Final
          , tzmCreated = "2019-04-10"
          , tzmTitle = mkSafeHtml "TZIP Purpose and Guidelines"
          , tzmAuthor = "John Burnham, Jacob Arluck (@governancy), Julien Hamilton (@julien.hamilton)"
          , tzmType = "Meta"
          , tzmDiscourseTopicId = Just 43
          }
        , tzbDirectLink = "http://test-repo-url/proposals/tzip-1/tzip-1.md"
        })
      , (ProposalId "tzip-12", TzipBubble
        { tzbSummary = mkSafeHtml $
            "<p>TZIP-12 summary line 1 TZIP-12 summary line 2</p>"
        , tzbMeta = TzipMeta
          { tzmStatus = Draft
          , tzmCreated = "2020-01-24"
          , tzmTitle = mkSafeHtml "Test tzip - sample"
          , tzmAuthor = "John Doe"
          , tzmType = "Financial Application (FA)"
          , tzmDiscourseTopicId = Nothing
          }
        , tzbDirectLink = "http://test-repo-url/proposals/tzip-12/tzip-12.md"
        })
      , (ProposalId "tzip-13", TzipBubble
        { tzbSummary = mkSafeHtml $
            "<p>TZIP-13 summary line 1 TZIP-13 summary line 2</p>"
        , tzbMeta = TzipMeta
          { tzmStatus = Draft
          , tzmCreated = "2020-01-24"
          , tzmTitle = mkSafeHtml "Test tzip - sample"
          , tzmAuthor = "John Doe"
          , tzmType = "Financial Application (FA)"
          , tzmDiscourseTopicId = Nothing
          }
        , tzbDirectLink = "http://test-repo-url/proposals/tzip-13/tzip-13.md"
        })
      ]

    expectedDraftBubbleData =
      [ ("a-draft-proposal.md", DraftTzipBubble
        { dtzbSummary = mkSafeHtml "<p>A test summary</p>"
        , dtzbMeta = DraftTzipMeta
          { dtzmCreated = Just "2020-12-09"
          , dtzmTitle = mkSafeHtml "A draft proposal"
          , dtzmAuthor = "John Doe (johndoe@mail.com)"
          , dtzmDate = Just "2020-12-15"
          , dtzmType = "proposal"
          , dtzmVersion = Just "0"
          }
        , dtzbDirectLink = "http://test-repo-url/drafts/current/a-draft-proposal.md"
        })
      ]
  in [ goldenTest
       "Generator creates files as expected"
       (pure $ (S.fromList expectedGeneratedFiles, S.fromList expectedBubbleData, S.fromList expectedDraftBubbleData, testRepoUrl))
       (runGeneratorAndCollectOutput withSystemTempDirectory)
       (\gv tv -> pure $ if gv == tv then Nothing else Just "Unexpected generator output")
       (pure . const ())
     , let
          withSystemTempDirectoryPopulated template cb =
            withSystemTempDirectory template (\odir -> do
              let exDir = odir </> "proposal" </> "tzip-13"
              liftIO $ createDirectoryIfMissing True exDir
              shelly $ touchfile (exDir </> "dummyfile")
              cb odir)
       in goldenTest
       "Generator creates files as expected when output directory contains output from previous runs"
       (pure $ (S.fromList expectedGeneratedFiles, S.fromList expectedBubbleData, S.fromList expectedDraftBubbleData, testRepoUrl))
       (runGeneratorAndCollectOutput withSystemTempDirectoryPopulated)
       (\gv tv -> pure $ if gv == tv then Nothing else Just "Unexpected generator output")
       (pure . const ())
     ]



runGeneratorAndCollectOutput
  :: (forall m a. (MonadIO m, MonadMask m) => String -> (FilePath -> m a) -> m a)
  -> IO (S.Set FilePath, S.Set (ProposalId, TzipBubble), S.Set (FilePath, DraftTzipBubble), Text)
runGeneratorAndCollectOutput prepOutDirCallback = prepOutDirCallback testOutputTemplate
  $ \outputDirectory -> do
    generate testRepoPath outputDirectory testRepoUrl
    resultFiles <- findFilesInDir outputDirectory
    mIndexData <- eitherDecodeFileStrict @IndexProposals $ outputDirectory </> "proposals" <.> "json"
    case mIndexData of
      Left err ->  error ("Parsing of proposals.json failed with:" <> (toText err))
      Right idData -> pure $
        ( S.fromList $ (makeRelative outputDirectory) <$> resultFiles
        , S.fromList $ M.assocs $ ipBubbles $ idData
        , S.fromList $ M.assocs $ ipDraftBubbles $ idData
        , ipRepoUrl idData)

-- | Recursively list all files in a directory.
findFilesInDir :: FilePath -> IO [FilePath]
findFilesInDir = go
  where
    go dir = do
      entries <- listDirectory dir
      liftM concat $ forM entries $ \e -> do
        let path = dir </> e
        doesDirectoryExist path >>= \case
          False -> pure $ [path]
          True  -> go path

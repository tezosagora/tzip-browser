-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-License-Identifier: AGPL-3.0-or-later

-- | Gitlab Listener Server
module Server
  ( Config(..)
  , regeneratingFiles
  , server
  ) where

import Universum

import Data.Aeson
import Data.Aeson.Casing (aesonPrefix, snakeCase)
import qualified Data.Text as T
import Servant
import Servant.API ()
import Servant.Server ()
import qualified Shelly as S

import Generator (generate)
import Common (logMessage)

-----------------------------------------------
-- Types
-----------------------------------------------

data Config = Config
  { cGitlabToken :: Text
  , cOutputDir :: Text
  , cSourceDir :: Text
  , cRepoLink :: Text
  -- ^ `dir` that contain tzip repo
  -- Not including tzip itself, so if the tzip repo is in
  -- assets/tzip, `sourceDir` should be just `assets`
  , cIsListenOnUnixSocket :: Bool
  , cPort :: Int
  }

data SuccessResponse = SuccessResponse {
  sStatus :: String
  } deriving stock (Eq, Show, Generic)

instance ToJSON SuccessResponse where
  toJSON = genericToJSON $ aesonPrefix snakeCase

data GitlabCommit = GitlabCommit
  { gcAdded :: [Text]
  , gcModified :: [Text]
  , gcRemoved ::[Text]
  }  deriving stock (Eq, Show, Generic)

instance FromJSON GitlabCommit where
   parseJSON = genericParseJSON $ aesonPrefix snakeCase

data GitlabPushEvent = GitlabPushEvent
  { gRef :: Text
  , gCommits :: [GitlabCommit]
  }  deriving stock (Eq, Show, Generic)

instance FromJSON GitlabPushEvent where
   parseJSON = genericParseJSON $ aesonPrefix snakeCase

type ListenerAPI = "commits" :> "new"
  :> Header "X-Gitlab-Event" Text
  :> Header "X-Gitlab-Token" Text
  :> ReqBody '[JSON] GitlabPushEvent
  :> Post '[JSON] SuccessResponse

-----------------------------------------------
-- API
-----------------------------------------------

listenerAPI :: Config -> Server ListenerAPI
listenerAPI = commitsNewHandler

server :: Config -> Application
server = serve listenerProxy . listenerAPI
  where
    listenerProxy :: Proxy ListenerAPI
    listenerProxy = Proxy

-----------------------------------------------
-- Listen to new commits
-----------------------------------------------

commitsNewHandler :: Config -> Maybe Text -> Maybe Text -> GitlabPushEvent -> Handler SuccessResponse
commitsNewHandler config gEvent gToken body = do
  logMessage ("[commits/new]: Got new request:" <> (show body))

  case (gEvent, validToken config <$> gToken) of
    (Just "Push Hook", Just True) ->
      case (isMasterBranch body, isNewCommitAffectSubscribedDir body) of
        (True, True) -> do
          logMessage "[commits/new]: Changes detected. Re-generating files"

          -- TODO: Since gitlab expect fast response from webhook
          -- we should run this in background case of it taking too long
          liftIO (regeneratingFiles config)

          pure (SuccessResponse "Running re-generating in background")
        _ -> do
          logMessage "[commits/new]: No changes detected."
          pure (SuccessResponse "No Updates")
    _ -> do
      logMessage "[commits/new]: Invalid headers"
      throwError err403

-----------------------------------------------
-- Helpers
-----------------------------------------------

validToken :: Config -> Text -> Bool
validToken config requestToken =
  cGitlabToken config == requestToken

isNewCommitAffectSubscribedDir :: GitlabPushEvent -> Bool
isNewCommitAffectSubscribedDir body =
  gCommits body
    & fmap (\GitlabCommit{..} -> gcAdded <> gcRemoved <> gcModified)
    & intercalate []
    & fmap (\filePath ->
        subscribedDir
          & fmap (\dir -> (dir <> "/") `T.isInfixOf` filePath)
          & or
        )
    & or

  where
    subscribedDir = ["proposals"]

isMasterBranch :: GitlabPushEvent -> Bool
isMasterBranch body =
  gRef body == "refs/heads/master"

regeneratingFiles :: Config -> IO ()
regeneratingFiles Config{..} = do
  logMessage "[generate job]: Pulling new changes from the repository"
  S.shelly $ do
    S.cd repoPath
    S.run_ "git" ["config", "pull.rebase", "true"]
    S.run_ "git" ["pull", "origin", "master"]

  generate repoPath (toString cOutputDir)
    $ fromMaybe (cRepoLink <> "/-/blob/master") Nothing
  logMessage "[generate job]: Generating completed"
  where
    repoPath = toString cSourceDir <> "/tzip"
